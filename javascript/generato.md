> #Generator介绍及使用

##生成器与迭代器简介

 **Generator（生成器）**是ES6标准引入的一种新的数据类型。它允许自定一个包含有迭代算法的函数，同时它能够自动维护自己的状态。

> **GeneratorFucntion**是一个可以作为迭代器工厂的特殊函数。当它被执行时会返回一个新的Generator对象。如果使用**funcion***语法，则函数变为GeneratorFunction。
> 
> **Generator**对象必须符合可迭代协议和迭代器协议。
> 
>#####可迭代协议
>
>可迭代协议允许Javascript对象去定义或定制他们的迭代行为，例如在一个for..of结构中什么值可以被消费。原生的Array及Map等对象都是内置的可迭代对象。但是Object则是不可迭代对象。如果想要变成可迭代对象，必须要实现**@@iterator**方法，即这个对象或它的**prototype chain**上必须有有一个名字是**Symbol.iterator**的属性，它是一个函数，调用这个函数时通常返回一个迭代器，通常每次调用都会返回一个全新的迭代器。在可迭代对象中，for..of会循环自动调用**Symbol.iterator**这个函数来构建迭代器，我们也可以手工的调用它，然后使用它返回的迭代器。
>
>		cosnt a = [1, 3, 4, 8]; 
>		const it = a[Symbol.iterator]();
>		it.next().value //1
>		it.next().value //3`
>
>#####迭代器协议
>
>迭代器协议定义了一种标准的方式来产生一个有限或无限序列的值，当一个对象被认为是一个迭代器时，它必须实现一个名为next的接口方法，该方法返回一个对象，这个对象有两个属性，**done**是一个布尔值，表示迭代器的完成状态，value中放置迭代值。
>
>通过迭代器协议和可迭代协议我们可以创造我们自己的迭代器接口：
>
>		const someThings = (function() {
>			let nextValue;
>
>			return {
>				//for..of循环需要
>				[Symbol.iterator]:funtion(){return this},
>			
>			   //迭代器接口方法
>				next:function() {
>					if(nextValue === undefined) {
>						nextValue = 1;
>					} else {
>						nextValue++;
>					}
>				
>					return {done: false, value: nextValue}
>				}
>			}
>		})();
>		
>		someTings.next().value() //1
>		someTings.next().value() //2
>		...
>		
>		for(var it of someThings) {
>			console.log(it)
>			//避免死循环
>			if(it > 100) {
>				break;
>			}
>		}
>		//1 2 3 4...
>
>##生成器
>
>生成器本身并不是一个iterable,但是当执行一个生成器函数，我们就会得到一个迭代器，生成器的名字大多也是来自于这种生产消费值的用例：
>
>			function* generatorId() {
>				let i = 0;
>				while(true){
>					yield i;
>					i++;
>				}
>			}
>			
>			const it = generatorId();
>			it.next().value //0
>			it.next().value //1
>			...
>
>###生成器的内部构成
>
> 示例：
> 
> ```
> 	function* somethingGenerator() {
> 		yield 'hello'
> 	}
> 	const it = somethingGenerator();
> 	it.next() // {value:'hello', done: false}
> 	it.next() // {value: undefined, done: true}
> 
> ```
>这个示例中我们知道调用一个生成器函数，不会立即执行它，而是创建了一个新的迭代器，通过该迭代器我们才能从生成器中请求值。在生成器生成一个值后，生成器会挂起执行并等待下一个请求的到来。从某一方面说，生成器更像是一个在状态中运行的状态机。
>
>
>1. **const it = somethingGenerator()**
>	* 创建生成器从挂起状态开始 
>2. **it.next()**
>	* 激活生成器，从挂起状态转为执行状态。执行到**yield**语句中停止，从而转为挂起让渡状态。返回新的对象**{value:hello, done:false}**
>3. 再次执行**it.next()**又重新激活了生成器。从挂起让渡状态又转成执行状态，没有代码可以执行，转为完成状态。这里返回新的对象**{value:undefined, done:true}**
>
>⚠️ <em>细心的同学可能会发现，yield和next()调用不匹配，或者说需要调用的next()总会比yield语句多一个。根据我们的视角不同，这里其实是没有不匹配的，这个稍后再说。</em>
>
>在**JavaScript**中，执行上下文环境是用于跟踪函数执行的js内部机制。一旦发生函数调用，当前执行上下文必须停止，并创建新的函数执行上下文来执行函数，还会创建一个与之关联的词法环境（词法环境是js内部用于跟踪标识符与特定变量之间的映射关系，词法环境就是js作用于的内部实现机制，通常我们叫它作用域。），js引擎将调用函数的内置**[[Environment]]**属性与创建时的环境进行关联（这也说明了函数作用域与在何处调用无关，是在声明的时候就已经确定了）。并把当前函数执行上下文推入到函数调用栈中，当函数执行完毕之后，当前函数从函数调用栈中弹出，当前执行环境销毁。生成器也是一种函数，但是它执行过后，他的上下文环境没有及时销毁，相反，调用next()时会重新激活对应的上下文环境。思考一下上面示例中**generatorId**这个id生成器函数，当我们调用**GeneratorId** 函数：
>
>```const it = generatorId() ```
>
>控制流进入生成器，和进入其他函数一样，当前会创建一个新的函数环境上下文，并将该上下文入栈。但是生成器比较特殊，它不会执行任何函数代码，取而代之的是生成一个新的迭代器从中返回，通过**it**引用这个返回的迭代器。由于迭代器是用来控制生成器执行的，所以迭代器**it**中保存着一个在它创建位置处执行的上下文。通常，只要保证函数对创建它的环境持有一个引用，这样就可以形成一个闭包。对于生成器来说，它必须要恢复执行，由于所有函数的执行都被上下文所控制，所以迭代器保持了一个对当前执行环境的引用。当调用迭代器**next()**方法的时候，如果是一个普通函数，它会创建一个新的执行上下文，但是迭代器不同，它会重新激活对应的执行上下文，从它上次离开的地方继续执行。在产生了一个值后，生成器的执行环境就会从栈中弹出，但是不会销毁，在本例中，**it**保存着对它的引用，所以它不会销毁，生成器挂进入挂起让渡状态，这也就保持了变量的状态，其原理和闭包类似。当遇到另一个next调用时，代码继续运行，知道遇到return语句，生成器进入了结束状态。
>
>###关于next与yield不匹配
>
>我们从迭代器的角度来考虑这个问题,在somethingGenerator这个生成器中，
>
>```
>	const it = somethingGenerator();
>	it.next(); // 'hello'
>```
>这个**it.next()**可以看成它在提出一个问题：生成器***somethingGenerator()**要给我的下一个值是什么？谁来回答这个问题呢？**yield 'hello'**表达式，当我们再次调用**it.next()**的时候还是提出相同的问题，生成器将要产生的值是什么？但是没有**yield**语句来回答这个问题，那么谁来回答呢？是**return**语句。虽然我们的***somethingGenerator（）**中没有return语句，这一点生成器函数和普通函数没有区别，return语句不是必须的，总是有一个隐士的return，它会默认情况下回答下一个**it.next()**提出的问题。
>
>到此，生成器与迭代器的概念大多介绍完了，还有很多用法及api大家可以参考 [MDN官方文档](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Statements/function*)；关于生成器最让兴奋的特性就是可以将异步代码可以写成看似同步的代码，如果结合**Promise**的可信任性和可组合行，那么我们的异步代码将会是可控的以及易维护的。包括**ES7 中的async/await**,本质上就是**Generator 和Promise**的语法糖而已。
>
>***
>
>###总结
>生成器是一种不会在同时输出所有值序列的函数，而是给予每次的请求生成值。生成器可以在当前运行状态中暂停，并在需要的时候恢复，生成器的暂停与恢复是合作式的，生成器具有独一无二的能力来暂停自身，这是通过yield关键字来实现的，然后通过迭代器next()方法来恢复执行。在异步流程控制方面，生成器为异步代码保持了顺序、同步、阻塞的代码模式。和生成器有关的异步中间件比较有关是**redux-saga**, 我们可以代替**redux-chunk**来书写优雅的异步**Action**,这是[redux-saga](https://redux-saga-in-chinese.js.org/)的介绍,关于生成器的应用实例我会写在下面。
>
>
>####遍历dom树
>
>
>
>```
><div id="subTree">
>	<div>Div</div>
>	<span>Span</span>
>	<p>p</p>
>	<div>
>		<a>13444</a>
>	</div>
></div>
>
><script>
>	function* DomTraversal(ele) {
>		yield ele;
>		ele = ele.firstElemtChild;
>	
>		while(ele) {
>			yield *DomTraversal(ele);
>			ele = ele.nextElementSibling;
>		}
>	}
>	const subTree = documets.querySelector('#subTree');
>	for(let ele of DomTraversal(subTree)) {
>			console.log(ele)
>	}
></scirpt>
>
>```
>
>####Generator与Promise结合
>	生成器该如何与Promise结合呢？生成器返回的迭代器应该能够监听Promise的状态，resolve或者reject，通过Prosmise的状态来恢复或者暂停生成器。最简单的方式就是yield出来一个Promise, 然后在通过这个Promise来来控制生成器的代码。	
>
>```
>	//ajax
>	const request = url =>  {
>		return new Promise((resolve, reject) => {
>				const xhr = new XMLHttpRequest();
>				xhr.open('GET', url);						
>				xhr.onload = function () {
>					try {
>						if(this.status === 200) {
>						resovle(JSON.parse(this.response))
>	
>						} else {
>							reject(this.staus + ' ' +this.statusText);
>						}
>					} catch (e) {
>						reject(e.message);
>					}
>					
>					xhr.onerror = function () {
>							reject(this.staus + ' ' +this.statusText);

>					}
>
>					xhr.send();
>
>				}
>
>		})
>	}
>	
>	//generator与promise结合
>	const async(gen) {
>		const it = gen();
>		
>		function handle(itRes) {
>			if(itRes.done) return;
>			
>			let itValue = itRes.value;
>			
>			if(itValue instanceof Promise) {
>				itValue.then(res => hanlde(it.next(res)))
>				.catch(err => it.throw(err));
>	
>			}
> 
>```